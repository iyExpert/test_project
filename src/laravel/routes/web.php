<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register' => false]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

#### ACCOUNT ####
Route::group(['middleware' => ['role:customer'], 'prefix' => 'account', 'namespace' => 'App\Http\Controllers\Account'], function () {
    Route::get('', 'AccountController@index')->name('account.index');
    Route::get('{user}/edit', 'AccountController@edit')->name('account.edit');
    Route::put('{user}', 'AccountController@update')->name('account.update');
    Route::delete('{user}', 'AccountController@destroy')->name('account.destroy');

    //News
    Route::group(['prefix' => 'news'], function () {
        Route::get('', 'NewsController@index')->name('account.news.index');
    });
});
#### /ACCOUNT ####

#### ADMIN ####
Route::group(['middleware' => ['role:admin'], 'prefix' => 'admin', 'namespace' => 'App\Http\Controllers\Admin'], function () {
    Route::get('', 'AdminController@index')->name('admin.index');

    Route::resource('news', 'NewsController');
});
#### /ADMIN ####
