<?php
namespace App\Services\DTO;

class NewsDTO extends BaseDto
{
    public string $title;
    public string $description;
    public bool $new_image = false;
}
