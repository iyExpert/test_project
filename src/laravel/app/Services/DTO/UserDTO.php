<?php
namespace App\Services\DTO;

use Illuminate\Http\UploadedFile;

class UserDTO extends BaseDto
{
    public string $name;
    public string $email;
    public UploadedFile|null $avatar = null;
}
