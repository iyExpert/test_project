<?php

namespace App\Services;

use App\Models\News;
use App\Services\DTO\NewsDTO;
use Illuminate\Pagination\LengthAwarePaginator;

class NewsService
{
    /**
     * @var News
     */
    protected News $model;

    /**
     * @var UnsplashService
     */
    protected UnsplashService $unsplashService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(News $model, UnsplashService $unsplashService)
    {
        $this->model = $model;
        $this->unsplashService = $unsplashService;
    }

    /**
     * @return Mixed
     */
    public function filter(): Mixed
    {
        return $this->model->orderBy('id', 'desc')->simplePaginate(10);
    }

    public function store(NewsDTO $newsDTO): News
    {
        $data = $newsDTO->toArray();
        if($data['new_image']){
            $data['image'] = $this->unsplashService->randomImage();
        }

        return $this->_save($this->model, $data);
    }

    /**
     * @param News $news
     * @param NewsDTO $newsDTO
     * @return News
     */
    public function update(News $news, NewsDTO $newsDTO): News
    {
        $data = $newsDTO->toArray();
        if($data['new_image']){
            $data['image'] = $this->unsplashService->randomImage();
        }

        return $this->_save($news, $data);
    }

    /**
     * @param News $news
     * @param array $data
     * @return News
     */
    private function _save(News $news, array $data): News
    {
        $news->fill($data);
        $news->save();

        return $news;
    }

    /**
     * @param News $news
     * @return bool|null
     */
    public function destroy(News $news): ?bool
    {
        return $news->delete();
    }
}
