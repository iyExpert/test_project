<?php

namespace App\Services;

use Unsplash\HttpClient;
use Unsplash\Photo;

/**
 * Service for work with Unsplash API
 *
 * ToDO: add check init and other methods
 */
class UnsplashService
{
    /**
     * UnsplashService constructor
     */
    public function __construct()
    {
        HttpClient::init([
            'applicationId'	=> config('services.unsplash.applicationId'),
            'secret' => config('services.unsplash.secret'),
            'callbackUrl' => config('services.unsplash.callbackUrl'),
            'utmSource' => config('services.unsplash.utmSource')
        ]);
    }

    /**
     * Get path random image
     *
     * @param string $query
     * @return string
     */
    public function randomImage(string $query = 'coffee'): string
    {
        $photo = Photo::random(['query' => $query, 'count' => 1])->toArray();
        return $photo[0]['urls']['regular'] ?? '';
    }
}
