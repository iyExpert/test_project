<?php

namespace App\Services;

use App\Models\User;
use App\Services\DTO\UserDTO;

class UserService
{
    /**
     * @param User $user
     * @param UserDTO $userDTO
     * @return User
     */
    public function update(User $user, UserDTO $userDTO): User
    {
        $data = $userDTO->toArray();
        $fileName = time() . '.' . $data['avatar']->extension();
        $data['avatar']->storeAs('public/images', $fileName);
        $data['avatar'] = $fileName;

        $user->fill($data);
        $user->save();
        return $user;
    }

    /**
     * @param User $user
     * @return bool|null
     */
    public function destroy(User $user): ?bool
    {
        return $user->delete();
    }
}
