<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\NewsFormRequest;
use App\Http\Resources\NewsResource;
use App\Models\News;
use App\Services\DTO\NewsDTO;
use App\Services\NewsService;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    /**
     * @var NewsService
     */
    protected NewsService $service;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(NewsService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('News.index', ['news' => $this->service->filter()]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('News.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(NewsFormRequest $request, NewsDTO $newsDTO)
    {
        $status = 'Record successfully created';

        try {
            $this->service->store($newsDTO->createFromArray($request->validated()));
        }catch (\Throwable $e){
            $status = $e->getMessage();
        }

        return redirect()->route('news.index')->with('status', $status);
    }

    /**
     * Display the specified resource.
     */
    public function show(News $news)
    {
        return view('News.show', ['news' => $news]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(News $news)
    {
        return view('News.edit', ['news' => $news]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(NewsFormRequest $request, NewsDTO $newsDTO, News $news)
    {
        $status = 'Record successfully updated';

        try {
            $this->service->update($news, $newsDTO->createFromArray($request->validated()));
        }catch (\Throwable $e){
            $status = $e->getMessage();
        }

        return redirect()->back()->with('status', $status);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(News $news)
    {
        $status = 'Record successfully deleted';

        try {
            $this->service->destroy($news);
        }catch (\Throwable $e){
            $status = $e->getMessage();
        }

        return redirect()->back()->with('status', $status);
    }
}
