<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use App\Models\News;
use App\Services\NewsService;

class NewsController extends Controller
{
    /**
     * @var NewsService
     */
    protected NewsService $service;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(NewsService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('News.index', ['news' => $this->service->filter()]);
    }
}
