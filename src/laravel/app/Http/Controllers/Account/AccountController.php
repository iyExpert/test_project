<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserFormRequest;
use App\Models\User;
use App\Services\DTO\UserDTO;
use App\Services\UserService;
use Illuminate\Contracts\View\View;

class AccountController extends Controller
{
    /**
     * @var UserService
     */
    protected UserService $service;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserService $service)
    {
        $this->service = $service;
    }

    /**
     * @return View
     */
    public function index(): View
    {
        return view('account.index');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(User $user)
    {
        return view('account.edit', ['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UserFormRequest $request, UserDTO $userDTO, User $user)
    {
        $status = 'Record successfully updated';

        try {
            $this->service->update($user, $userDTO->createFromArray($request->validated()));
        }catch (\Throwable $e){
            $status = $e->getMessage();
        }

        return redirect()->back()->with('status', $status);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(User $user)
    {
        $status = 'Record successfully deleted';

        try {
            $this->service->destroy($user);
        }catch (\Throwable $e){
            $status = $e->getMessage();
        }

        return redirect()->back()->with('status', $status);
    }
}
