<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('email', 'admin@site.com')->first();
		if(!$user){
			$user = User::create([
				'name'     => 'admin',
				'email'    => 'admin@site.com',
				'password' => bcrypt('testAdmin2023'),
			]);
		}
        $user->syncRoles(['admin']);

        $user = User::where('email', 'test_user@site.com')->first();
		if(!$user){
            $user = User::create([
				'name'     => 'testUser',
				'email'    => 'test_user@site.com',
				'password' => bcrypt('testUser2023'),
			]);
		}
        $user->syncRoles(['customer']);
    }
}
