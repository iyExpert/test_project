<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RolesTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()['cache']->forget('spatie.permission.cache');

        $adminRole = Role::where('name', 'admin')->first();
        if(!$adminRole){
            Role::create(['name' => 'admin']);
        }

        $customerRole = Role::where('name', 'customer')->first();
        if(!$customerRole){
            Role::create(['name' => 'customer']);
        }
    }
}
