<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if (session()->has('status'))
                <div class="alert alert-info" role="alert">
                    {{ session()->get('status') }}
                </div>
            @endif

            @if (isset($errors) AND count($errors->toArray()) > 0)
                <div class="alert alert-danger" role="alert">
                    <ul>
                        @foreach($errors->toArray() as $error)
                            @foreach($error as $errMsg)
                                <li>{{$errMsg}}</li>
                            @endforeach
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>
</div>
