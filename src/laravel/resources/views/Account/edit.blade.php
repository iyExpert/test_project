@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><a href="{{route('account.index')}}"><</a> {{ __('Edit User') }} "{{$user->name}}"</div>

                <div class="card-body">
                    <form method="POST" action="{{route('account.update', ['user' => $user->id])}}" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <div class="mb-3">
                            <label for="name" class="form-label">Name</label>
                            <input type="text" class="form-control" id="name" placeholder="User Name" name="name" value="{{ old('name') ?? $user->name }}">
                        </div>
                        <div class="mb-3">
                            <label for="email" class="form-label">email</label>
                            <input type="email" class="form-control" id="email" placeholder="User Email" name="email" value="{{ old('email') ?? $user->email }}">
                        </div>
                        <div class="mb-3">
                            <label for="avatar" class="form-label">Avatar</label>
                            <input type="file" id="avatar" name="avatar">
                            @if(\Illuminate\Support\Facades\Storage::disk('public')->exists('images/'.$user->avatar))
                                <img src="/storage/images/{{$user->avatar}}" alt="{{$user->name}}" style="max-width: 120px">
                            @endif
                        </div>
                        <div class="mb-3">
                            <input type="submit" value="Save" class="btn btn-success">
                            <a href="{{route('account.index')}}" class="btn btn-info">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
