@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Account') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                    <br/><br/>
                    <b>Name:</b> {{auth()->user()->name}}<br/>
                    <b>e-mail:</b> {{auth()->user()->email}}
                    <br/><br/>
                    @if(\Illuminate\Support\Facades\Storage::disk('public')->exists('images/'.auth()->user()->avatar))
                        <img src="/storage/images/{{auth()->user()->avatar}}" alt="{{auth()->user()->name}}" style="max-width: 120px">
                    @endif
                    <br/><br/>
                    <a href="{{route('account.edit', ['user' => auth()->user()->id])}}" class="btn btn-info">Edit</a>
                    <form method="POST" action="{{route('account.destroy', ['user' => auth()->user()->id])}}" style="display: inline">
                        @method('DELETE')
                        @csrf
                        <input type="submit" value="Delete" class="btn btn-danger">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
