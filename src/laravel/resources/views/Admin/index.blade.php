@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Admin') }}</div>

                <div class="card-body">
                    {{ __('Admin. You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
