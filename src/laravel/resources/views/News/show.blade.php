@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><a href="{{route('news.index')}}"><</a> "{{$news->title}}"</div>

                <div class="card-body">
                    <b>Title: </b>{{$news->title}}<br/>
                    <b>Description: </b><br/>{{$news->description}}<br/>
                    <img src="{{$news->image}}" alt="{{$news->title}}" style="width: 200px"><br/><br/><br/>

                    <a href="{{route('news.index')}}" class="btn btn-info">Go to List</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
