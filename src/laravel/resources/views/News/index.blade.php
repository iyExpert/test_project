@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    {{ __('News') }}
                    @if(auth()->user()->hasRole('admin'))
                        <a href="{{route('news.create')}}" title="Add News">+</a>
                    @endif
                </div>

                <div class="card-body">
                    @if($news->count() > 0)
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col" style="width: 30px">#</th>
                                    <th scope="col" style="width: 75px">Image</th>
                                    <th scope="col">Title</th>
                                    @if(auth()->user()->hasRole('admin'))
                                        <th scope="col" style="width: 225px">Actions</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($news as $item)
                                    <tr>
                                        <td>{{$item->id}}</td>
                                        <td class="text-center"><img src="{{$item->image}}" alt="" style="width: 90%" /></td>
                                        <td>{{$item->title}}</td>

                                        @if(auth()->user()->hasRole('admin'))
                                            <td>
                                                <a href="{{route('news.show', ['news' => $item->id])}}" class="btn btn-success">Show</a>
                                                <a href="{{route('news.edit', ['news' => $item->id])}}" class="btn btn-info">Edit</a>
                                                <form method="POST" action="{{route('news.destroy', ['news' => $item->id])}}" style="display: inline">
                                                    @method('DELETE')
                                                    @csrf
                                                    <input type="submit" value="Delete" class="btn btn-danger">
                                                </form>
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="alert alert-info" role="alert">
                            Records not found
                        </div>
                    @endif

                    <div class="d-flex justify-content-center">
                        {{$news->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
