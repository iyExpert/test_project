@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><a href="{{route('news.index')}}"><</a> {{ __('Add News') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{route('news.store')}}">
                        @csrf
                        <input type="hidden" id="new_image" name="new_image" value="true">
                        <div class="mb-3">
                            <label for="title" class="form-label">Title</label>
                            <input type="text" class="form-control" id="title" placeholder="News Title" name="title" value="{{ old('title') }}">
                        </div>
                        <div class="mb-3">
                            <label for="description" class="form-label">Description</label>
                            <textarea class="form-control" id="description" rows="3" name="description">{!! old('description') !!}</textarea>
                        </div>
                        <div class="mb-3">
                            <input type="submit" value="Add" class="btn btn-success">
                            <a href="{{route('news.index')}}" class="btn btn-info">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
